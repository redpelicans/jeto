# jeto

A nodeJS library to ease initialisation of resources at boot time.

# Context

We've seen to much projects that start by executing a huge boilerplate close to index.js to initialize resources like database, express, http servers, backends conx and global objects used later by server's code (cache, factories, ...)

We published [evtX](https://www.npmjs.com/package/evtx) and use it all over our NodeJS projets to organize code between expressJS middlewares (routes) and services implementations.

`jeto` is the complement of  [evtX](https://www.npmjs.com/package/evtx), it helps to build a global context step by step at boot time to initialize resources required by services, ideally evtX services!


It's a very small piece of code inspired by `React#setState` and [flyd](https://www.npmjs.com/package/flyd) api.


# Usage (see example folder in repo)

At first we need to define our resources:

```
import initConfig from "./init/config";
import initDatabase from "./init/database";
import initRouter from "./init/router";
import initHttp from "./init/http";

const resources = [initConfig, initDatabase, initRouter, initHttp];

```

A `resource initializer` is a function that returns a jeto's context or a Promise with may be a jeto's context as value.

```
export default ctx => ctx({ config: { database: url } } });
};

or 

export default ctx => {
  const { config: { database: { url } } } = ctx();
  return Promise.resolve(ctx({ database: new Database().connect(url) }));
};

```


Then, import a helper to load all resources and load them to get a context:

```

import { initResources } from 'jeto';

initResources(resources)
   .then(ctx)
   .then(console.log(`server started`)

```


Here you have organized resources boot chain


# API

## createContext(initialContext): Jeto's context

create a jeto's context from initialContext
returns a jeto's context

## Get context values

A jeto's context is a function, just call it to get current context values
```
import { createContext } from 'jeto';

const ctx = createContext({ a: 1})
expect(ctx()).toEqual({ a: 1})
```

## Update a context

Call context function with updates, call is synchronous:

```
const ctx = createContext({ a: 1})
ctx({ b: 2 })
expect(ctx()).toEqual({ a: 1, b: 2})
```

## Initialize resources

This is the main use case, see above ...


## Listen to updates

Last but not the least a context is an eventEmitter

On each ctx(value) call an 'update' event is published

```
  ctx = createContext(initialContext);
  const updates = { b: 1 };
  ctx.on("update", ({ prevContext, updates }) => {
    expect(prevContext).toEqual(initialContext);
    expect(updates).toEqual(updates);
  });
  ctx(updates);
```


That's all folks ...