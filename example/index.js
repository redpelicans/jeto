const { initResources } = require("..");
const initConfig = require("./init/config");
const initDatabase = require("./init/database");
const initRouter = require("./init/router");
const initHttp = require("./init/http");

const resources = [initConfig, initDatabase, initRouter, initHttp];

initResources(resources)
  .then(ctx => {
    const {
      config,
      httpServer: { url }
    } = ctx();
    console.log(config);
    console.log(`server listen on ${url}`);
  })
  .catch(console.error);
