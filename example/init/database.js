class Database {
  connect() {
    return Promise.resolve({ status: "ok" });
  }
}

module.exports = ctx => {
  const {
    config: {
      database: { url }
    }
  } = ctx();
  return Promise.resolve(ctx({ database: new Database().connect(url) }));
};
