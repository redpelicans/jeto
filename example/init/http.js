module.exports = ctx => {
  const {
    app,
    config: {
      server: { host }
    }
  } = ctx();
  return new Promise((resolve, reject) => {
    app.listen(host, err => {
      if (err) return reject(err);
      resolve(ctx({ httpServer: { url: `https://${host}:5555` } }));
    });
  });
};
