const App = () => ({
  listen: (host, cb) => setTimeout(() => cb(), 200)
});

module.exports = ctx => {
  const app = App();
  // app.use(...)
  return ctx({ app });
};
