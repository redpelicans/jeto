"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initConfigResources = exports.initResources = exports.createContext = void 0;

var _events = _interopRequireDefault(require("events"));

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);
  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    if (enumerableOnly)
      symbols = symbols.filter(function(sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    keys.push.apply(keys, symbols);
  }
  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};
    if (i % 2) {
      ownKeys(source, true).forEach(function(key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(source).forEach(function(key) {
        Object.defineProperty(
          target,
          key,
          Object.getOwnPropertyDescriptor(source, key)
        );
      });
    }
  }
  return target;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

var createContext = function createContext(initialContext) {
  var em = new _events["default"]();
  var context = initialContext;

  var ctx = function ctx(updates) {
    if (updates) {
      var newContext = _objectSpread({}, context, {}, updates);

      em.emit("update", {
        prevContext: context,
        updates: updates
      });
      context = newContext;
      return ctx;
    }

    return context;
  };

  ctx.on = function() {
    return em.on.apply(em, arguments);
  };

  return ctx;
};

exports.createContext = createContext;

var initResources = function initResources(resources) {
  var initialContext =
    arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return resources.reduce(function(acc, initFn) {
    return acc.then(initFn);
  }, Promise.resolve(createContext(initialContext)));
};

exports.initResources = initResources;

var initConfigResources = function initConfigResources(resources) {
  return function(config) {
    return initResources(resources, config);
  };
};

exports.initConfigResources = initConfigResources;
