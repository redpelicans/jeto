import EventEmitter from "events";

export const createContext = initialContext => {
  const em = new EventEmitter();
  let context = initialContext;
  const ctx = updates => {
    if (updates) {
      const newContext = { ...context, ...updates };
      em.emit("update", { prevContext: context, updates });
      context = newContext;
      return ctx;
    }
    return context;
  };
  ctx.on = (...params) => em.on(...params);
  return ctx;
};

export const initResources = (resources, initialContext = {}) =>
  resources.reduce(
    (acc, initFn) => acc.then(initFn),
    Promise.resolve(createContext(initialContext))
  );
export const initConfigResources = resources => config =>
  initResources(resources, config);
