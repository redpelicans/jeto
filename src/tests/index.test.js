import { createContext, initResources, initConfigResources } from "..";

let ctx;
const initialContext = { a: 1 };

describe("", () => {
  it("should create a context", () => {
    ctx = createContext(initialContext);
    expect(ctx()).toEqual(initialContext);
  });

  it("should update context", () => {
    const updates = { b: 1 };
    expect(ctx(updates)()).toEqual({ ...ctx(), ...updates });
  });

  it("should init chained resources started from raw object", () => {
    const initConfig = () => Promise.resolve(initialContext);
    const initR1 = ctx => ctx({ b: 2 });
    const initR2 = ctx => ctx({ b: 3, c: 3 });
    const resources = [initR1, initR2];
    return initConfig()
      .then(initConfigResources(resources))
      .then(ctx => expect(ctx()).toEqual({ a: 1, b: 3, c: 3 }));
  });

  it("should init chained resources", () => {
    const initConfig = ctx => ctx(initialContext);
    const initR1 = ctx => ctx({ b: 2 });
    const initR2 = ctx => Promise.resolve(ctx({ b: 3, c: 3 }));
    const resources = [initConfig, initR1, initR2];
    return initResources(resources).then(ctx =>
      expect(ctx()).toEqual({ a: 1, b: 3, c: 3 })
    );
  });

  it("should emit update event", done => {
    ctx = createContext(initialContext);
    const updates = { b: 1 };
    ctx.on("update", ({ prevContext, updates }) => {
      expect(prevContext).toEqual(initialContext);
      expect(updates).toEqual(updates);
      done();
    });
    ctx(updates);
  });
});
